#!/bin/bash
#SBATCH --job-name=Si_wall_2x2_3rd_IFCs
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=bd740@york.ac.uk
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=06:00:00
#SBATCH --account=PHYS-CASTEP-2019
#SBATCH --array=1-200
#SBATCH --output=array.log
module load phys/CASTEP/19.1.1-foss-2018b

for i in $(seq $SLURM_ARRAY_TASK_ID 200 42636)
do                                                                                                                                                                             
    job=`printf "job-%05d" $i`
    cd Si_wall-3RD/$job
    castep.mpi Si_wall
    cd -
    echo "job-$i finished" >> jobs_done.txt
done  
