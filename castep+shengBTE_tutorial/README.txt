This directory contains CASTEP interface files to the ShengBTE package for the solution of the Boltzmann Transport Equation for phonons. 
This package is NOT part of CASTEP and the CASTEP authors are not responsible for it.
You can find further information on ShengBTE, including features and downloads, at http://www.shengbte.org/

