#!/bin/bash

#This script should allow compilation of all the extra tools required for thermal conductivity calculations on viking.
# - if compiling not on viking comment out the module load commands, but double check there is a mpi compiler using the command "mpif90 -v"
# - this should give the version information for gfortran

# Go into spglib directory and build library
cd spglib

mkdir _build
cd _build
cmake -DCMAKE_INSTALL_PREFIX="" ../
make 
make DESTDIR=../lib install

cd ../

echo
echo "CHECK OUTPUT ABOVE FOR ERRORS, OTHERWISE LOCAL VERSION OF SPGLIB SHOULD NOW BE INSTALLED"
echo

#spglib should be installed now, just need to:

#link include file to directory called spglib so stupid thirdorder setup.py can find it
ln -s lib/include/spglib.h spglib.h


#Proceed to compilation of thirdorder

cd ../thirdorder

./compile.sh

echo
echo "CHECKOUT OUPUT ABOVE FOR ERRORS (not warnings), OTHERWISE THIRDORDER_CASTEP.PY SHOULD NOW BE INSTALLED"
echo

#Make note of what directory we are in for instructions
cwd=$(pwd)

cd ../ShengBTE-v1.1.1/Src/
make all
cd ../../

#link executable to thirdorder directory so it can be found when added to the apath
ln -s ShengBTE-v1.1.1/ShengBTE thirdorder/ShengBTE


echo
echo "IMPORTANT"
echo
echo "IN ORDER TO USE thirdorder_castep.py, PLEASE ADD THE FOLLOWING LINE TO THE END OF THE FILE '~/.bashrc', VIA 'emacs -nw ~/.bashrc' OR 'nano ~/.bashrc'"
echo "export PATH=\$PATH:"$cwd"/" 
echo
echo "THEN RUN 'source ~/.bashrc' "
echo
