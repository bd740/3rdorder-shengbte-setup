export FFLAGS=-O3 -funroll-loops -fno-signed-zeros -g -fbacktrace -march=native -fconvert=big-endian -fno-realloc-lhs -fPIC 
export LDFLAGS=-L../../spglib/lib/lib/ -Wl,-rpath=../../spglib/lib/lib/  -lsymspg
export MPIFC=mpif90
MKL=$(MKLROOT)/lib/em64t/libmkl_lapack95_lp64.a -Wl,--start-group	\
$(MKLROOT)/lib/em64t/libmkl_intel_lp64.a				\
 $(MKLROOT)/lib/em64t/libmkl_sequential.a				\
 $(MKLROOT)/lib/em64t/libmkl_core.a -Wl,--end-group -lpthread -lm
export LAPACK=-llapack -lopenblas
export LIBS=$(LAPACK)
